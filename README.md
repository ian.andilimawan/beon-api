## How to Install

1. Run `composer install` in your terminal to install the required dependencies.
2. Type in the terminal `cp .env.example .env` to copy the environment configuration file.
3. Set up your database configuration in the `.env` file according to your local environment.
4. Generate an application key by typing `php artisan key:generate` in the terminal.
5. Run database migrations by typing `php artisan migrate` in the terminal.
6. Optionally, you can seed the database with initial data by typing `php artisan db:seed`.
7. Start the Laravel development server by typing `php artisan serve` in the terminal.
