<?php

namespace App\Enums;

use Symfony\Component\HttpFoundation\Response;

enum ResponseCode
{
    case STATUS_OK;

    case STATUS_CREATED;

    case STATUS_BAD_REQUEST;

    case STATUS_UNPROCESSABLE_ENTITY;

    case STATUS_NOT_FOUND;

    case STATUS_FORBIDEN;

    case STATUS_UNATENTICATED;

    case STATUS_INTERNAL_SERVER_ERROR;

    public function getCode(): int
    {
        return match ($this) {
            ResponseCode::STATUS_OK => Response::HTTP_OK,

            ResponseCode::STATUS_CREATED => Response::HTTP_CREATED,

            ResponseCode::STATUS_BAD_REQUEST => Response::HTTP_BAD_REQUEST,

            ResponseCode::STATUS_UNPROCESSABLE_ENTITY => Response::HTTP_UNPROCESSABLE_ENTITY,

            ResponseCode::STATUS_NOT_FOUND => Response::HTTP_NOT_FOUND,

            ResponseCode::STATUS_FORBIDEN => Response::HTTP_FORBIDDEN,

            ResponseCode::STATUS_UNATENTICATED => Response::HTTP_UNAUTHORIZED,

            ResponseCode::STATUS_INTERNAL_SERVER_ERROR => Response::HTTP_INTERNAL_SERVER_ERROR,
        };
    }
}
