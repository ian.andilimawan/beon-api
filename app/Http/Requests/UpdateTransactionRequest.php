<?php

namespace App\Http\Requests;

use App\Helpers\ValidatorCustom;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTransactionRequest extends ValidatorCustom
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "id_occupant"=> "string|nullable",
            "transaction_status"=> "string",
            "description" => "string",
            "total" => "numeric|required",
            "contribution" => "string|nullable",
            "type" => "string|nullable",
            "status" => "string",
            "monthly_fees" => "date"
        ];
    }
}
