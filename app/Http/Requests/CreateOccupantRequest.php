<?php

namespace App\Http\Requests;

use App\Helpers\ValidatorCustom;

class CreateOccupantRequest extends ValidatorCustom
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'phone_number' => 'required',
            'marriage' => 'required',
            'status' => 'required',
            'id_card' => 'required|file|image|mimes:png,jpg,jpeg'
        ];
    }
}
