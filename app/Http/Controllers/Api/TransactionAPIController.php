<?php

namespace App\Http\Controllers\Api;

use App\Enums\ResponseCode;
use App\Helpers\ResponseJson;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Services\Api\TransactionAPIService;

class TransactionAPIController
{
    public function __construct(private TransactionAPIService $transactionAPIService) {}

    public function index()
    {
        $data = $this->transactionAPIService->listTransaction();

        return ResponseJson::make(ResponseCode::STATUS_OK, 'Data retrieved successfully', $data)->send();
    }

    public function show(int $id)
    {
        $transaction = $this->transactionAPIService->getTransactionById($id);

        if (!$transaction['status']) {
            return ResponseJson::make(ResponseCode::STATUS_NOT_FOUND, 'Data not found')->send();
        }

        return ResponseJson::make(ResponseCode::STATUS_OK, $transaction['message'], $transaction['data'])->send();
    }

    public function store(CreateTransactionRequest $request)
    {
        try {
            $this->transactionAPIService->createTransaction($request->all());
            return ResponseJson::make(ResponseCode::STATUS_CREATED, 'Create data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function update(int $id, UpdateTransactionRequest $request)
    {
        try {
            $this->transactionAPIService->updateTransaction($id, $request->all());
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Update data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->transactionAPIService->deleteTransaction($id);
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Delete data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }
}
