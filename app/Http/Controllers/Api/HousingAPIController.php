<?php

namespace App\Http\Controllers\Api;

use App\Enums\ResponseCode;
use App\Helpers\ResponseJson;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateHousingRequest;
use App\Http\Requests\UpdateHousingRequest;
use App\Services\Api\HousingAPIService;
use Illuminate\Http\Request;

class HousingAPIController extends Controller
{
    public function __construct(private HousingAPIService $housingAPIService){}

    public function index()
    {
        $data = $this->housingAPIService->listHousing();

        return ResponseJson::make(ResponseCode::STATUS_OK, 'Data retrieved successfully', $data)->send();
    }

    public function show(int $id)
    {
        $housing = $this->housingAPIService->getHousingById($id);

        if (!$housing['status']) {
            return ResponseJson::make(ResponseCode::STATUS_NOT_FOUND, $housing['message'])->send();
        }

        return ResponseJson::make(ResponseCode::STATUS_OK, $housing['message'], $housing['data'])->send();
    }

    public function store(CreateHousingRequest $request)
    {
        try {
            $this->housingAPIService->createHousing($request->all());
            return ResponseJson::make(ResponseCode::STATUS_CREATED, 'Create data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function update(int $id, UpdateHousingRequest $request)
    {
        try {
            $this->housingAPIService->updateHousing($id, $request->all());
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Update data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->housingAPIService->deleteHousing($id);
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Delete data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }
}
