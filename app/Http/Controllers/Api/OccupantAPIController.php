<?php

namespace App\Http\Controllers\Api;

use App\Enums\ResponseCode;
use App\Helpers\ResponseJson;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOccupantRequest;
use App\Http\Requests\UpdateOccupantRequest;
use App\Services\Api\OccupantAPIService;
use Illuminate\Http\Request;

class OccupantAPIController extends Controller
{
    public function __construct(private OccupantAPIService $occupantAPIService){}

    public function index()
    {
        $data = $this->occupantAPIService->listOccupant();

        return ResponseJson::make(ResponseCode::STATUS_OK, 'Data retrieved successfully', $data)->send();
    }

    public function show(int $id)
    {
        $occupant = $this->occupantAPIService->getOccupantById($id);

        if (!$occupant['status']) {
            return ResponseJson::make(ResponseCode::STATUS_NOT_FOUND, $occupant['message'])->send();
        }

        return ResponseJson::make(ResponseCode::STATUS_OK, $occupant['message'], $occupant['data'])->send();
    }

    public function store(CreateOccupantRequest $request)
    {
        try {
            $this->occupantAPIService->createOccupant($request->all());
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Create data successfully')->send();
        } catch (\Throwable $th) {
            return ResponseJson::make(ResponseCode::STATUS_INTERNAL_SERVER_ERROR, $th->getMessage())->send();
        }
    }

    public function update(int $id, UpdateOccupantRequest $request)
    {
        try {
            $this->occupantAPIService->updateOccupant($id, $request->all());
            return ResponseJson::make(ResponseCode::STATUS_OK,'Updated successfully')->send();
        } catch (\Throwable $th) {
           return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->occupantAPIService->deleteOccupant($id);
            return ResponseJson::make(ResponseCode::STATUS_OK, 'Delete data successfully')->send();
        } catch (\Throwable $th) {
           return ResponseJson::make($th->getCode(), $th->getMessage())->send();
        }
    }

    public function getAll()
    {
        $data = $this->occupantAPIService->allOccupant();

        return ResponseJson::make(ResponseCode::STATUS_OK, 'Data retrieved successfully', $data)->send();
    }
}
