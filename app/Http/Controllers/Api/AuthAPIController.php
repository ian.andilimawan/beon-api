<?php

namespace App\Http\Controllers\Api;

use App\Enums\ResponseCode;
use App\Helpers\ResponseJson;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Services\Api\AuthAPIService;
use Illuminate\Http\Request;

class AuthAPIController extends Controller
{

    public function __construct(private AuthAPIService $authAPIService) {}

    public function doLogin(LoginRequest $request)
    {
        $response = $this->authAPIService->loginService($request);

        if (!$response['status']) {
            return ResponseJson::make(ResponseCode::STATUS_BAD_REQUEST, $response['message'])->send();
        }

        return ResponseJson::make(ResponseCode::STATUS_OK, $response['message'], $response['data'])->send();
    }
}
