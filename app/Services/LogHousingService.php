<?php

namespace App\Services;
use App\Repositories\LogHousingRepository;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;

class LogHousingService
{
    public function __construct(private LogHousingRepository $housingRepository) {}

    public function listLogHousing(): Paginator
    {
        return $this->housingRepository->listLogHousing();
    }

    public function createLogHousing(array $data): void
    {
        try {
            $this->housingRepository->createLogHousing($data);
        } catch (\Throwable $th) {
            throw new Exception("Failed create data");
        }
    }
}
