<?php

namespace App\Services\Api;

use App\Repositories\HousingRespository;
use App\Services\LogHousingService;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;

class HousingAPIService
{
    public function __construct(private HousingRespository $housingRespository, private LogHousingService $logHousingService){}

    public function listHousing(): Paginator
    {
        return $this->housingRespository->listHousing();
    }

    public function getHousingById(int $id): array
    {
        $data = $this->housingRespository->getHousingById($id);

        if (!$data) {
            return [
                'status' => false,
                'message' => 'Data not found'
            ];
        }

        return [
            'status' => true,
            'message' => 'Data retrieved successfully',
            'data' => $data
        ];
    }

    public function createHousing(array $data): void
    {
        try {
            $respCreateData = $this->housingRespository->createHousing($data);
            $log = [
                'id_housing' => $respCreateData->id,
                'id_occupant' => $respCreateData->id_occupant,
                'status' => $respCreateData->status == 'inhabited' ? 'in' : 'out'
            ];
            $this->logHousingService->createLogHousing($log);
        } catch (\Throwable $th) {
            throw new Exception("Failed create data");
        }
    }

    public function deleteHousing(int $id): bool
    {
        $data = $this->housingRespository->getHousingById($id);

        if (!$data) {
            throw new Exception("Data not found", 404);
        }

        return $this->housingRespository->deleteHousing($data);
    }

    public function updateHousing(int $id, array $request): bool
    {
        $data = $this->housingRespository->getHousingById($id);

        if (!$data) {
            throw new Exception("Data not found", 404);
        }

        if ($data['id_occupant'] != $request['id_occupant']) {
            $logIn = [
                'id_housing' => $data->id,
                'id_occupant' => $request['id_occupant'],
                'status' => 'in'
            ];

            $logOut = [
                'id_housing' => $data->id,
                'id_occupant' => $data->id_occupant,
                'status' => 'out'
            ];

            $this->logHousingService->createLogHousing($logIn);
            $this->logHousingService->createLogHousing($logOut);
        } else {
            $log = [
                'id_housing' => $data->id,
                'id_occupant' => $request['id_occupant'],
                'status' => 'in'
            ];

            $this->logHousingService->createLogHousing($log);
        }


        return $this->housingRespository->updateHousing($data, $request);
    }
}
