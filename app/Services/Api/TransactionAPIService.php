<?php

namespace App\Services\Api;

use App\Repositories\TransactionRepository;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;

class TransactionAPIService
{
    public function __construct(private TransactionRepository $transactionRepository) {}

    public function listTransaction(): Paginator
    {
        return $this->transactionRepository->listTransaction();
    }

    public function getTransactionById(int $id): array
    {
        $data = $this->transactionRepository->getTransactionById($id);

        if (!$data) {
            return [
                'status' => false,
                'message' => 'Data not found'
            ];
        }

        return [
            'status' => true,
            'message' => 'Data retrieved successfully',
            'data' => $data
        ];
    }

    public function createTransaction(array $data): void
    {
        try {
            $this->transactionRepository->createTransaction($data);
        } catch (\Throwable $th) {
            throw new Exception('Failed create data');
        }
    }

    public function deleteTransaction(int $id): bool
    {
        $data = $this->transactionRepository->getTransactionById($id);

        if (!$data) {
            throw new Exception('Data not found', 404);
        }

        return $this->transactionRepository->deleteTransaction($data);
    }

    public function updateTransaction(int $id, array $request): bool
    {
        $data = $this->transactionRepository->getTransactionById($id);

        if (!$data) {
            throw new Exception('Data not found',404);
        }

        return $this->transactionRepository->updateTransaction($data, $request);
    }
}
