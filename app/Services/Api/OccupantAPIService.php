<?php

namespace App\Services\Api;

use App\Repositories\OccupantRepository;
use Exception;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\UploadedFile;

class OccupantAPIService
{
    public function __construct(private OccupantRepository $occupantRepository) {}

    public function listOccupant(): Paginator
    {
        return $this->occupantRepository->listOccupant();
    }

    public function getOccupantById(int $id): array
    {
        $data = $this->occupantRepository->findOccupantById($id);

        if (!$data) {
            return [
                'status' => false,
                'message' => 'Data not found'
            ];
        }

        return [
            'status' => true,
            'message' => 'Data retrieved successfully',
            'data' => $data
        ];
    }

    public function createOccupant(array $request): void
    {
        try {
            if (isset($request['id_card']) && $request['id_card'] instanceof UploadedFile) {
                $file = $request['id_card'];
                $file->storeAs('public/id_card', $file->hashName());
                $request['id_card'] = 'id_card/'.$file->hashName();
            }
            $this->occupantRepository->createOccupant($request);
        } catch (\Throwable $th) {
            throw new Exception("Failed create data");
        }
    }

    public function deleteOccupant(int $id): bool
    {
        $data = $this->occupantRepository->findOccupantById($id);

        if (!$data) {
            throw new Exception("Data not found", 404);
        }

        return $this->occupantRepository->deleteOccupant($data);
    }

    public function updateOccupant(int $id, array $request): bool
    {

        $data = $this->occupantRepository->findOccupantById($id);

        if (!$data) {
            throw new Exception("Data not found", 404);
        }

        if (isset($request['id_card']) && $request['id_card'] instanceof UploadedFile) {
            $file = $request['id_card'];
            $file->storeAs('public/id_card', $file->hashName());
            $request['id_card'] = 'id_card/'.$file->hashName();
        }

        return $this->occupantRepository->updateOccupant($data, $request);
    }

    public function allOccupant()
    {
        return $this->occupantRepository->allOccupant();
    }
}
