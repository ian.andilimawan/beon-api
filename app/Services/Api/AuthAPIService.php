<?php

namespace App\Services\Api;

use App\Repositories\AuthRepository;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class AuthAPIService
{
    public function __construct(private UserRepository $userRepository) {}

    public function loginService(Request $request)
    {
        $user = $this->userRepository->findByEmail($request->email);

        if (!$user) {
            return [
                'status' => false,
                'message' => 'Email or password not match'
            ];
        }

        if (!Hash::check($request['password'], $user->password)) {
            return [
                'status' => false,
                'message' => 'Email or password not match'
            ];
        }

        $token = $user->createToken('appToken')->plainTextToken;


        return [
            'status'=> true,
            'message' => 'Login successfully',
            'data' => [
                'user' => $user,
                'token' => $token
            ]
        ];
    }
}
