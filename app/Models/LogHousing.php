<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class LogHousing extends Model
{
    use HasFactory, HasApiTokens;

    protected $table = 'log_housing';

     protected $fillable = [
        'id_occupant',
        'id_housing',
        'status',
     ];

     protected $hidden = [
        'created_at',
        'updated_at',
     ];
}
