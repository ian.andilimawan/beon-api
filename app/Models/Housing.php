<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

class Housing extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    protected $table = 'housing';

    protected $fillable = [
        'id_occupant',
        'house_number',
        'status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function details(): HasOne
    {
        return $this->hasOne(Occupant::class, 'id', 'id_occupant');
    }
}
