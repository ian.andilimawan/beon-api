<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

class Occupant extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    protected $table = 'occupant';

    protected $fillable = [
        'fullname',
        'id_card',
        'phone_number',
        'marriage',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
