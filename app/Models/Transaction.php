<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;

class Transaction extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    protected $table = 'transaction';

    protected $fillable = [
        'id_occupant',
        'transaction_status',
        'description',
        'total',
        'contribution',
        'type',
        'status',
        'monthly_fees'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function details_occupant(): HasOne
    {
        return $this->hasOne(Occupant::class, 'id', 'id_occupant');
    }
}
