<?php

namespace App\Exceptions;

use Throwable;
use App\Helpers\ResponseJson;
use App\Dto\ResponseWrapperDto;
use App\Enums\ResponseCode;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
        });
    }

    public function render($request, Throwable $exception)
    {
        Log::info('error'. $exception->getMessage(), ['exception'=> $exception]);

        if ($exception instanceof AuthenticationException) {
            return ResponseJson::make(ResponseCode::STATUS_UNATENTICATED, $exception->getMessage())->send();
        }

        return ResponseJson::make(ResponseCode::STATUS_INTERNAL_SERVER_ERROR, 'internal server error', $error = [
            'message' => 'Access Denied'
        ])->send();
    }
}
