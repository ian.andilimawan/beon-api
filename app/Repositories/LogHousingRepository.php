<?php

namespace App\Repositories;

use App\Models\LogHousing;
use Illuminate\Contracts\Pagination\Paginator;

class LogHousingRepository
{
    public function __construct(private LogHousing $model) {}

    public function listLogHousing(): Paginator
    {
        return $this->model->paginate(10);
    }

    public function createLogHousing(array $data): ?LogHousing
    {
        return $this->model->create($data);
    }
}
