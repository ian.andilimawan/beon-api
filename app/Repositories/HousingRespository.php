<?php

namespace App\Repositories;

use App\Models\Housing;
use Illuminate\Contracts\Pagination\Paginator;

class HousingRespository
{
    public function __construct(private Housing $model) {}

    public function listHousing(): Paginator
    {
        return $this->model->with('details')->paginate(10);
    }

    public function getHousingById(int $id): ?Housing
    {
        return $this->model->find($id);
    }

    public function createHousing(array $data): Housing
    {
        return $this->model->create($data);
    }

    public function deleteHousing(Housing $housing): bool
    {
        return $housing->delete();
    }

    public function updateHousing(Housing $housing, array $request): bool
    {
        return $housing->update($request);
    }
}
