<?php

namespace App\Repositories;

use App\Models\Transaction;
use Illuminate\Contracts\Pagination\Paginator;

class TransactionRepository
{
    public function __construct(private Transaction $model) {}

    public function listTransaction(): Paginator
    {
        return $this->model->with('details_occupant')->paginate(10);
    }

    public function getTransactionById(int $id): ?Transaction
    {
        return $this->model->find($id);
    }

    public function createTransaction(array $data): Transaction
    {
        return $this->model->create($data);
    }

    public function deleteTransaction(Transaction $transaction): bool
    {
        return $transaction->delete();
    }

    public function updateTransaction(Transaction $transaction, array $request): bool
    {
        return $transaction->update($request);
    }
}
