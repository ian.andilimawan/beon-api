<?php

namespace App\Repositories;

use App\Models\Occupant;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class OccupantRepository
{
    public function __construct(private Occupant $model) {}

    public function listOccupant(): Paginator
    {
        return $this->model->paginate(10);
    }

    public function findOccupantById(int $id): ?Occupant
    {
        return $this->model->find($id);
    }

    public function createOccupant(array $data): Occupant
    {
        return $this->model->create($data);
    }

    public function deleteOccupant(Occupant $occupant): bool
    {
        return $occupant->delete();
    }

    public function updateOccupant(Occupant $occupant, array $request): bool
    {
        return $occupant->update($request);
    }

    public function allOccupant(): Collection
    {
        return $this->model->all();
    }
}
