<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->integer('id_occupant')->nullable(true);
            $table->enum('transaction_status', ['expense','income']);
            $table->text('description')->nullable(true);
            $table->float('total');
            $table->enum('contribution', ['monthly','year'])->nullable(true);
            $table->enum('type', ['security','cleanliness'])->nullable(true);
            $table->enum('status', ['paid_off','not_paid_off'])->nullable(true);
            $table->date('monthly_fees')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
