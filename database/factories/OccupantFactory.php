<?php

namespace Database\Factories;

use App\Models\Occupant;
use Illuminate\Database\Eloquent\Factories\Factory;

class OccupantFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Occupant::class;

    public function definition()
    {
        return [
            'fullname' => $this->faker->name(),
            'phone_number' => $this->faker->phoneNumber(),
            'marriage' => $this->faker->boolean(),
            'status' => $this->faker->boolean(),
        ];
    }
}
