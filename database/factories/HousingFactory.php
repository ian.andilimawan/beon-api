<?php

namespace Database\Factories;

use App\Models\Occupant;
use Illuminate\Database\Eloquent\Factories\Factory;

class HousingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "house_number" => $this->faker->randomDigitNotNull(),
            "id_occupant" => Occupant::all()->random()->id,
            "status" => $this->faker->randomElement(['inhabited','not_inhabited']),
        ];
    }
}
