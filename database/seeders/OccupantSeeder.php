<?php

namespace Database\Seeders;

use App\Models\Occupant;
use Illuminate\Database\Seeder;

class OccupantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Occupant::truncate();
        Occupant::factory(10)->create();
    }
}
