<?php

use App\Http\Controllers\Api\AuthAPIController;
use App\Http\Controllers\Api\HousingAPIController;
use App\Http\Controllers\Api\OccupantAPIController;
use App\Http\Controllers\Api\TransactionAPIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [AuthAPIController::class, 'doLogin']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::apiResource('occupation', OccupantAPIController::class);
    Route::apiResource('housing', HousingAPIController::class);
    Route::apiResource('transaction', TransactionAPIController::class);

    Route::get('all-occupant', [OccupantAPIController::class, 'getAll']);
});
